(ns meetup.main
  (:require [kee-frame.core :as kf]
            [re-frame.core :as rf]))

(rf/reg-sub
 ::title
 (fn [db _]
   (:title db)))

(rf/reg-event-db
 ::update-title
 (fn [db [_ title]]
   (assoc db :title title)))

(defn app []
  (let [title (rf/subscribe [::title])]
    (fn []
      [:div.welcome
       [:h1 @title]
       [:input {:type :text
                :value @title
                ;; e.target.value
                :on-change (fn [e] (rf/dispatch [::update-title (.-value (.-target e))]))}]])))

(defn main! []
  (kf/start! {:routes [["/" :route/main]]
              :initial-db {:title "Hello Madison!"}
              :root-component [app]})
  (println "Loaded"))
