# shadow-cljs from scratch

## prerequisites
  - jdk 8+
  - node 10+
  - yarn (recommended, not required)

## to start

fetch dependencies

`yarn`

then

`yarn shadow-cljs watch :app`

then navigate to http://localhost:3000 in your browser.

 ## to release

 `yarn shadow-cljs release :app`

This will produced an optimized build for production.

## learn more

- [shadow-cljs](http://shadow-cljs.org/)
- [re-frame](https://github.com/Day8/re-frame/blob/master/docs/README.md)
